## Being an employee

- *What is her/his gross monthly salary? You may give a range.*

  **TODO**

- *Based on the gross monthly salary, how much can he/she expect to receive every month?*

  **TODO**

- *How much vacation money is he/she entitled to?*

  **TODO**

- *Assuming the company is paying all of its employees a 13th month (bonus), show how you calculate his/her gross yearly salary.*

  **TODO**

- *Does any CAO apply on this employee? If so, which one?*

  **TODO**

- *Is the employer required to pay for a pension plan? Why (not)?*

  **TODO**

- *Assuming the employer *is* providing a pension plan, what would be a ball-park figure that would be put into the pension plan for this employee every month? Would he/she typically pay part of that? If so, what may be a reasonable split?*

  **TODO**

- *If this employee was not living up to the company's expectations, even after providing lots of time an opportunities to improve, what options does the company have? What are the options for firing somebody, and when can they be used? Any options besides that?*

  **TODO**

- *When this employee was first hired, would he likely have gotten a permanent or a temporary contract? Why? How many temporary contracts would be allowed, and for what period of time?*

  **TODO**

- *Suppose the employee receives a brand new Volkswagen Golf (non-electric, no options) as a lease car. Give a ballpark figure of what this would cost the employer per year (excluding fuel costs), and give an approximate calculation for the additional income tax the employee would have to pay based on the *bijtelling*. *

  **TODO**
