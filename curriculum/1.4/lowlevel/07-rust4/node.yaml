name: Polymorphism
description: Smart pointers and dyn traits.
goals:
    rust-polymorphism: 1
    memheap: 1
resources:
    -
        link: https://doc.rust-lang.org/stable/book/ch15-00-smart-pointers.html
        title: The Rust Book - Smart Pointers
        info: You'll only need to read the **introduction and section 15.1 and 15.2**.
    -
        link: https://doc.rust-lang.org/book/ch17-00-oop.html
        title: Object Oriented Programming Features of Rust
        info: Read the **introduction and sections 17.1 and 17.2**.
assignment:
    - In *1.2 Algorithms -> Priority queues* you implemented a binary search tree. Let's do that again, but this time using Rust.
    -
        title: A binary search tree for `String`s.
        ^merge: feature
        text: |
            Implement a binary search tree that works for the `String` data type. It should implement the following methods:

            - `new` to create a new binary search tree,
            - `add` to add a String to the tree,
            - `contains` to check if a String is already in the tree, and
            - `as_vector` to return a sorted `Vec<String>`.

            Create unit tests that test all branches of your code.

            Hints:

            - As a tree is a recursive data structure, you'll need to allocate child nodes on the heap.
            - A (sub)tree can be modelled as an `enum` with two options:
                - Empty.
                - Not empty, in which case there should be attributes for value, left and right.
            - You can use `match` to [destructure enums](https://doc.rust-lang.org/book/ch18-03-pattern-syntax.html#destructuring-enums).

    -
        title: A binary search tree for cars and paintings
        ^merge: feature
        text: |
            Start in a new Rust file.

            Now let's supposed a filthy rich client offers you a lot of money to create a digital catalogue for her most valuable possessions: sports cars and paintings. The catalogue should at any time be sorted by the value of each object, starting with the most expensive object (of course!). So let's use a binary search tree for that!

            The problem is that we need to store different information for both types of valuables. Paintings have an artist, a title and the current price. Sports cars have a brand, a model, an original price and an age (in years). The current price of the car is calculated as `original_price * (0.95 ^ age_in_years)`. (Floating point numbers have a `powf` method.) Implement these as `struct`s.

            Create an `Valuable` trait that provides methods for getting the current price of the valuable and for getting a description. Implement this trait for both of your `struct`s.
            
            Create a copy of your binary search tree, and modify it so that it stores `Valuable`s, ordered by descending price. Within the `main` function, insert a few cars and a few paintings into the tree, and use `as_vector` to print a list showing descriptions and current prices, demonstrating that items are stored in the tree in the right order.

            Hint: you'll need to use *trait objects*. As the size for a trait object cannot be known at compile-time (sports cars will take a bit more memory than paintings, because of an extra field), they can only be allocated on the heap.
