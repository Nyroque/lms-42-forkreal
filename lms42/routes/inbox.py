from ..app import db, app
from ..assignment import Assignment
from ..models import curriculum
from ..models.attempt import Attempt, Grading
from ..models.user import User, TimeLog
from ..utils import role_required, utc_to_display, local_to_utc
from flask_login import  current_user
import flask
import random
import sqlalchemy as sa
import datetime


QUEUE_DEADLINE = datetime.time(10,00)


def get_ungraded_attempts(for_class, queue_only: bool):
    """Create an SQLAlchemy query that returns all attempts that need grading.

    Args:
        for_class (str|bool):
            str: Only return attempts for students in the given class.
            True: Sort attempts primarily by the student's class.
            False: Ignore class.
        queue_only (bool): When True, only attempts to be graded today are queried.

    Returns: the query object.
    """
    
    # Select all attempts that need attention (including deadlines)
    ungraded_attempts = (
        Attempt.query
        .filter(sa.or_(
            Attempt.status.in_(("needs_grading","needs_consent","awaiting_approval",)),
            sa.and_(Attempt.status == "in_progress", Attempt.deadline_time < datetime.datetime.utcnow())
        ))
        .join(Attempt.student)
        .filter_by(level = 10)
        .filter_by(is_active = True)
        .filter_by(is_hidden = False)
    )

    # Filter if we're only showing today's grading queue
    if queue_only:
        today = datetime.datetime.utcnow().date()
        ExamAttempt = sa.orm.aliased(Attempt)
        ungraded_attempts = (
            ungraded_attempts
            .outerjoin(TimeLog, sa.and_(TimeLog.user_id == User.id, TimeLog.date==today))
            .filter(sa.or_(
                sa.and_(
                    TimeLog.start_time <= QUEUE_DEADLINE,
                    Attempt.submit_time < local_to_utc(datetime.datetime.combine(today, QUEUE_DEADLINE)),
                ),
                Attempt.status.in_(("needs_consent","awaiting_approval",)),
                sa.and_(Attempt.status == "in_progress", Attempt.deadline_time < datetime.datetime.utcnow()),
            ))
            .outerjoin(ExamAttempt, User.current_attempt_id == ExamAttempt.id)
            .filter(sa.or_(
                ExamAttempt.id == None,
                ExamAttempt.credits == 0,
            ))
        )

    # Order the results
    if queue_only:
        order_by = [Attempt.status != "in_progress", TimeLog.start_time]
    else:
        order_by = [Attempt.status != "in_progress", Attempt.submit_time, Attempt.deadline_time]

    if for_class == True: # Primarily sort by class
        order_by = [User.class_name] + order_by
    elif isinstance(for_class, str):
        pass
        # ungraded_attempts = ungraded_attempts.filter(User.class_name == for_class)

    return ungraded_attempts.order_by(*order_by)



@app.route('/inbox', methods=['GET'])
@role_required('teacher')
def grading_get_list():
    show = flask.request.args.get("show")
    if show:
        flask.session['inbox_show'] = 'today' if show == 'today' else 'all'
    show = flask.session.get('inbox_show', 'all')

    order = flask.request.args.get("order")
    if order:
        flask.session['inbox_order'] = 'class' if order == 'class' else 'date'
    order = flask.session.get('inbox_order', 'class')

    return flask.render_template('inbox.html',
        ungraded_attempts = get_ungraded_attempts(order=='class', show=='today'),
        order = order,
        show = show,
        nodes = curriculum.get('nodes_by_id'),
        modules = curriculum.get('modules_by_id'),
        online_user_ids = TimeLog.get_online_user_ids(),
    )


@app.route('/inbox/grade/<attempt_id>', methods=['POST'])
@role_required('teacher')
def grading_post(attempt_id):
    form = flask.request.form

    attempt = Attempt.query.get(attempt_id)

    if attempt.status == "in_progress":
        flask.flash("Attempt is still in progress!?")
        return flask.redirect("/inbox")
    if attempt.status != "needs_grading":
        flask.flash("Attempt was already graded! Your grading has replaced the old grading, but the old grading is still available in the database.")

    ao = Assignment.load_from_directory(attempt.directory)
    scores = ao.form_to_scores(form)
    motivations = ao.form_to_motivations(form)

    grade, passed = ao.calculate_grade(scores)

    # All exams with 5, 6 and 10 grades need approval, as well as a random 10% of the rest.
    needs_consent = "ects" in ao.node and (grade in [5,6,10] or random.randrange(0,10)==0 or bool(form.get('request_consent')))

    grading = Grading(
        attempt_id = attempt.id,
        grader_id = current_user.id,
        objective_scores = scores,
        objective_motivations = motivations,
        grade = grade,
        grade_motivation = form.get('motivation'),
        passed = passed,
        needs_consent = needs_consent,
    )


    if needs_consent:
        attempt.status = "needs_consent"
    elif "ects" in ao.node:
        attempt.status = "passed" if passed else "failed"
    elif form.get('formative_action') in ["failed","passed","repair"]:
        attempt.status = form.get('formative_action')
    else:
        attempt.status = "passed" if passed else "repair"

    db.session.add(grading)
    db.session.commit()

    attempt.write_json()

    student = attempt.student
    if attempt.status == "passed":
        flask.flash(f"{student.full_name} passed with a {round(grade)}!")
    elif attempt.status == "needs_consent":
        flask.flash("Grading needs consent from another teacher.")
        grading.send_consent_dm() # Send a dm to teachers to give grading consent. 
    elif grade == None:
        flask.flash(f"{student.full_name} has not received a grade.")
    else:
        flask.flash(f"{student.full_name} did NOT pass with a {round(grade)}.")
        grading.send_grade_dm() # Send a dm to the student with the failed grade.
    if "ects" in ao.node and attempt.status != "needs_consent":
        flask.flash(f"Remember to publish the grade in Bison! Don't finalize it yet. Work submitted on {utc_to_display(attempt.submit_time)}.")
        grading.send_grade_dm() # Send a dm to the student with the passed grade.
    
    return flask.redirect(f"/people/{attempt.student_id}#attempts")

