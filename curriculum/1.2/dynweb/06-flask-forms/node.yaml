name: Form handling
description: Handle forms and field validation.
goals:
    flask: 1
days: 2
resources:
    - In this lesson you'll learn how to use Flask-WTF to create forms with proper field validation and CSRF checks.
    -
        link: https://www.youtube.com/watch?v=-O9NMdvWmE8
        title: Python and Flask - Web Forms with Flask-WTF
        info: A video that covers web forms in flask using Flask-wtf.
    -
        link: https://flask-wtf.readthedocs.io/en/0.15.x/quickstart/
        title: Flask-WTF documentation - Quickstart
        info: Shows and explains a very minimal code example.
    -
        link: https://wtforms.readthedocs.io/en/2.3.x/fields/
        title: WTForms documentation - Fields
        info: Flask-WTF builds around WTForms (which is not Flask-specific). Therefore you'll need to refer to the WTForms documentation for most things. This chapters lists all available *field types* and how to configure them.
    -
        link: https://wtforms.readthedocs.io/en/2.3.x/validators/
        title: WTForms documentation - Validators
        info: This chapters lists all available *validators* types and how to configure them.


assignment:
    "Part 1: bug reporting":
        - |
            For this part of the assignment, there's an empty `part1` directory in the provided template. You'll need to create a new project yourself! (Hint: `poetry init -n`.)
        -
            ^merge: feature
            title: Bug report form
            text: | 
                Implement a bug report form using Flask-WTForms. It should have fields for:

                - Short description (10 to 60 characters).
                - Long (multi-line) description (at least 40 characters).
                - Priority (low, medium, high, critical).
                - Reporter email address (must be a valid email address).
                - Resolved (checkbox).

                Make sure that appropriate errors are reported in case validation conditions are not met. (What should happen in case the form is submitted correctly is specified later on.)

                **Hint:** You'll need to create a form class inheriting from `FlaskForm`, a Jinja2 template that renders that form, and a simple Flask route in order to instantiate a form and have it rendered by the template.

        -
            ^merge: feature
            title: Bug report system
            text: |
                Build a simple bug report system around your form, and a database wrapped by Flask-SQLAlchemy. It should have these pages:

                - Main page:
                    - Lists all bugs from the database, displayed as short descriptions. Clicking one opens its *edit bug page*.
                    - A *new bug* button, that opens the *new bug page*.
                - New bug page:
                    - It contains the form you created earlier.
                    - Saving should create a new bug in the database.
                - Edit bug page:
                    - It contains the same form (based on the same Jinja2 template).
                    - Values should be populated from the database.
                    - Saving should update the database.

                **Hint:** Make sure your WTForms forms use the same field names as your SQLAlchemy models. The way, you can save on some typing by instantiating form object like this `my_fancy_form = MyFancyForm(obj = my_fancy_model)` and by saving to the database like this `my_fancy_form.populate_obj(my_fancy_model)`. This comes instead of having to copy each of the fields manually.


        -
            link: https://flask-wtf.readthedocs.io/en/0.15.x/form/#file-uploads
            title: Flask-WTF documentation - File Uploads
            info: How to handle file uploads with Flask-WTF. Also notice the part mentioning `enctype="multipart/form-data"`!

        -
            title: Screenshot upload
            ^merge: feature
            text: |
                Add an extra field to the bug report: a screenshot. 
                
                - While creating a new bug report and while editing a bug report, the user should be able to upload a `.png` file (other file types should be rejected).
                - When the form is submitted and validated:
                    - The uploaded file should be stored in `static/screenshots/<filename>.png`, where `<filename>` is a randomly generated string (that is long enough that collisions are very unlikely).
                    - The random `<filename>` string should be stored in the bug report's database record, so that we know which screenshot belongs to which bug report.
                    - If a bug report already had a screenshot that is being replaced, the old file should be deleted.
                - The screenshot should be displayed on the *Edit bug page*.

        -
            title: Layout and styling
            1: Acceptable for an internal tool, no labels.
            2: Nice to look at, no labels
            3: Acceptable for an internal tool, with labels.
            4: Nice to look at, with labels.
            text: |
                All pages should be consistently laid out (using a Jinja2 layout template) and styled to a degree acceptable for an internal administration tool. For usability, you should display labels (and not just placeholder attributes) for each of the fields, so that a user can see the name of a field even when text has been entered.

                **Tip:** Invest some time now to create a `.css` file that allows you to make any kind of form look good in little time. You can then reuse this file in future lessons and even during the exam, to save time.


    "Part 2: generic form template":
        - |
            For this part of the assignment, there are some files provided in the `part2` directory of the provided template.
        -
            text: |
                When building an application that contains a lot of forms, creating the HTML templates for each for can be time consuming. If you're willing to compromise a bit on the aesthetics of the forms, it may be a good idea to create a generic form template - a Jinja2 template that tries its best to render any WTForms form you throw at it. Such a template should just loop over each of the form fields, drawing its label, its input and any errors attached to it.

                The provided `app.py` contains a form that should work well together with your generic form template. In `templates/generic-form.html` is something to get you started.

                Make sure you have proper handling for special cases such as the CSRF token, the submit button and the checkbox (which should of course have its label on the right).

                **Tip:** You can recycle the `.css` file you created earlier, and make sure it works well together with your generic form template. You are allowed to use both in future assignments.
            weight: 2
            ^merge: feature
