from ..app import db, app
from ..working_days import get_current_octant, get_octant_dates
from ..models.user import User, TimeLog
from ..models.attempt import Attempt
from ..routes.inbox import get_ungraded_attempts
from ..models import curriculum
import sqlalchemy as sa
import flask
import datetime

@app.route('/tv', methods=['GET'])
@app.route('/tv/<device>', methods=['GET'])
def tv(device=None):
    fragment = flask.request.args.get('fragment')
    if fragment in ('grading-queue',):
        template = fragment
    else:
        template = 'tv'

    ungraded_attempts = get_ungraded_attempts("ESD1V.{device}", True) if device in ('a','b',) else None
    
    return flask.render_template(template+'.html',
        header=False,
        device=device,
        ungraded_attempts=ungraded_attempts,
        order = "date",
        show = "today",
        nodes = curriculum.get('nodes_by_id'),
        modules = curriculum.get('modules_by_id'),
        online_user_ids = TimeLog.get_online_user_ids(),
    )
