name: Algorithms & complexity
goals:
    alg_sel: 1
resources:
    - link: https://www.youtube.com/watch?v=e_WfC8HwVB8
      title: University of Washington - What is an Algorithm?
    - link: https://www.youtube.com/watch?v=YoZPTyGL2IQ
      title: CS50 - Computational Complexity
      info: A good explanation of complexity. At the end, it will show some code examples in Java (instead of Python). Don't feel intimidated! In `for (int i = 0; i < 123; i++)` just means a loop that will run 123 times. Exactly like `for i in range(123)` in Python.
assignment:
    Assignment: |
        Let's imagine a *long* list of numbers. Two million of them. The numbers are ordered, but not (necessarily) consecutive. For example, the list could start with:
        
        ```
        [5, 8, 14, 20, 21, 30, 36, 36, 38, 41, .......]
        ```

        Now suppose someone gives you another list of numbers, and wants to know if and at what position each of the numbers occurs in the first (long!) list. We'll call the first (long!) list the haystack and the second list the needles. As the saying goes: to look for a needle in a haystack. In this case we're actually looking for a bunch of needles in a haystack.

        In case a needle occurs in the haystack more than once, we're interested in the position of just one of them (it doesn't matter which). 

        So given the needles `[14, 5, 7, 36, 37]` and the above haystack, the result would be either `[2, 0, None, 6, None]` or `[2, 0, None, 7, None]`.

        How would *you* implement this?

        Use the unittest to try your implementation of the different search algorithms.
        
        We'll be looking at three different algorithms for this.

    Test-Basics:
      -
        link: https://www.youtube.com/watch?v=1Lfv5tUGsn8
        title: Socratica - Unit Tests in Python
        info: A video introduction about why and how to use unit test in Python.

      - 
        text: |
          In the test_search.py there are several test cases for testing the correct functions of the search algorithm.  
          
        must: Test are running all with correct results.

    Linear search:
        - link: https://www.youtube.com/watch?v=TwsgCHYmbbA
          title: CS50 - Linear search
          info: Linear search is the simplest algorithm we'll be implementing. Keep in mind that the algorithm as explained will only search for a single needle, while we need to search for a bunch of needles.
        - text: |
            Implement your solution using a linear search. Do this in the provided `search.py` file, within the `linear_search` function. You should implement the search loop yourself, and *not* use a standard Python function such as `index()`.

            Test your solution with the follow command:
            ```
            python -m unittest test_search.Linear
            ```
          ^merge: feature
          map: 
            alg_sel: 1
    
    Dictionary search:
        - |
            Imagine you have a paper phone book. It's entries are sorted by city and then by name. Now what if you *often* want to lookup a name given a phone number? Reading the whole phone book every time is not ideal. One way would be to create a new phone book, that contains all the items, but ordered by phone number. This would obviously take a lot of work initially. But once you have the reverse phone book, looking up names given a phone number would be really quick!

            In Python, we could do something very similar. Except we use a `dict` instead of paper reverse phone book, where the keys would be the phone numbers and the values would be names. This way we could look up a phone number instantly, even if there were a many millions of numbers.
            
            Test your solution with the follow command:
            ```
            python -m unittest test_search.Dictionary
            ```

            To test if your execution time is faster than previous algorithm:
            ```
            python -m unittest test_search.ExecutionTime
            ```
        - text: Implement your solution using a dictionary search. Do this in the provided `search.py` file, within the `dictionary_search` function. Is it faster than the linear search?
          ^merge: feature
          map: 
            alg_sel: 1

    Binary search:
        - link: https://www.youtube.com/watch?v=T98PIp4omUA
          title: CS50 - Binary Search
          info: A more thorough explanation of binary search, which was already introduced in the *What is an Algorithm?* video.
        - text: |
            Implement your solution using binary search. You may assume that the haystack is already sorted. Do this in the provided `search.py` file, within the `binary_search` function. Is it faster than the dictionary search? 

            While it may be tempting to copy an implementation from the internet (or to be 'inspired' by it), there is a really good reason not to do that. Implementing binary search by yourself is a really valuable exercise. It requires exact reasoning. You need to be able to do this before we can get to the more advanced algorithms.

            Test your solution with the follow command:
            ```
            python -m unittest test_search.Binary
            ```

            To test if your execution time is faster than previous algorithm:
            ```
            python -m unittest test_search.ExecutionTime
            ```
          ^merge: feature
          map: 
            alg_sel: 1
        

    Complexity:
      -
        link: https://craigndave.org/wp-content/uploads/2020/09/big-o-notation-cheat-sheet.pdf
        title: Big O notation cheat sheet

      - |
        The image below shows how the big O looks like in the different notations. Use this as a cheat-sheet for determine the big O notation.
        <img src="graph_big_o.png" class="no-border" style="max-width: 40%; width: 40%; max-height: none;">
      -
        text: |
            In the provided `numeric.txt` file, you can see the run times for four unspecified algorithms. Please provide the big O time complexity for each.
        4: 4 correct
        map: 
            alg_sel: 1
      - 
        text: |
          For each of the three functions you implemented earlier, add the time complexity in the comments, expressed in terms of `h` (the number of items in the haystack) and `n` (the number of needles to search for). Note that big-O terms may be combined using addition (`+`) or multiplication (`*`).
        0: 0 correct
        1: 1 correct
        2: 2 correct
        3: 2 correct, 1 partly correct
        4: 3 correct
        map: 
            alg_sel: 1

      - text: |
            In the provided `analysis.txt` file, there is a *decision tree* meant to help programmers select which of the three algorithms to use. Please complete this tree by replacing each of the `???` placeholders with one of LINEAR, DICTIONARY and BINARY.
        0: 0-1 out of 6
        1: 2 out of 6
        2: 3-4 out of 6
        3: 5 out of 6
        4: 6 out of 6
        map: 
            alg_sel: 1
